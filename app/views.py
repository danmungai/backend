from flask import render_template
from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_appbuilder import ModelView, ModelRestApi
from flask_appbuilder import AppBuilder, expose, BaseView, has_access
from app import appbuilder
from . import appbuilder, db, app
from app.views import app   
from flask_appbuilder.models.sqla.interface import SQLAInterface
from .models import *
from flask_appbuilder.security.sqla.models import User
from flask_appbuilder.api import ModelRestApi


class HomeView(BaseView):
    route_base = "/home"

    @expose('/general/')
    def admin(self):
        greeting = "Hello Admin"
        return self.render_template('admin.html', greeting=greeting)


    @ expose('/admin/')
    def general(self):
        greeting = "Hello ordinary user"
        return self.render_template('index.html', greeting=greeting)

appbuilder.add_view_no_menu(HomeView())

class Admin(ModelView):
    datamodel = SQLAInterface(Admin)
    base_permissions = ['can_add','can_delete','can_list']

class MemberModelView(ModelView):
    datamodel = SQLAInterface(Member)
    label_columns = {'user_group':'User Group'}
    search_columns = ['FirstName','IDNumber']

    show_fieldsets = [
        (
            'personal information',
            {'fields': ['FirstName', 'SecondName', 'LastName', 'IDNumber', 'Address']}
        ),
        (
            'Employment Details',
            {'fields': ['Employer', 'EmployerAddress', 'Position', 'WorkStation', 'DateOfAppointment', 'GrossIncome', 'PayrollNumber']}
        )
    ]

class MemberModelApi(ModelRestApi):
    resource_name = 'member'
    datamodel = SQLAInterface(Member)
    show_columns = ['FirstName']



appbuilder.add_api(MemberModelApi)

class UserApi(ModelRestApi):
    resource_name = 'user'
    datamodel = SQLAInterface(User)
    show_columns = ['username']

appbuilder.add_api(UserApi) 

# class NomineeApi(ModelRestApi):
#     resource_name='nominee'
#     datamodel=SQLAInterface

# appbuilder.add_api(NomineeApi)

# class RefereeApi(ModelRestApi):
#     resource_name='referee'
#     datamodel=SQLAInterface   

# appbuilder.add_api(RefereeApi)
class LoanApi(ModelRestApi):
    resource_name = 'loan' 
    datamodel = SQLAInterface(Loan)
    show_columns = ['Amount']

appbuilder.add_api(LoanApi)



class GroupModelView(ModelView):
    datamodel = SQLAInterface(UserGroup)
    related_views =[MemberModelView]


db.create_all()
appbuilder.add_view(
    GroupModelView,
    "List user groups",
    icon = 'fa-folder-open-o',
    category = 'Members',
    category_icon= 'fa-envelope'
)


appbuilder.add_view(
    MemberModelView,
    "List members",
    icon = 'fa-envelope',
    category = "Members"
)

appbuilder.add_view(
    Admin,
    'Admin panel',
    icon = 'fa-user-plus',
    category = 'Admin',
)


@appbuilder.app.errorhandler(404)
def page_not_found(e):
    return (
        render_template(
            "404.html", base_template=appbuilder.base_template, appbuilder=appbuilder
        ),
        404,
    )


db.create_all()
