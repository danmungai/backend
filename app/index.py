from flask import g, url_for, redirect
from flask_appbuilder import IndexView, expose


class MyIndexView(IndexView):

    @expose('/')
    def index(self):
        """
        Gets User details and role, If the users role is index one, SuperAdmin, Its rerouted to the Admine page/html page
        """
        user = g.user

        if user.is_anonymous:
            return redirect(url_for('AuthDBView.login'))
        else:
            if user.roles == "1":
                return redirect(url_for('HomeView.admin'))
            else:
                return redirect(url_for('HomeView.general'))