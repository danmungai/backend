from flask_appbuilder import Model
from sqlalchemy import Column, Integer, String, ForeignKey, Date, Boolean, Table
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base


# Base = declarative_base()


class UserGroup(Model):
    id= Column(Integer, primary_key=True)
    name = Column(String(50), unique=True, nullable=False)

    def __repr__(self):
        return self.name

        
class Nominee(Model):
    id = Column(Integer, primary_key=True)
    Name = Column(String, nullable=False)
    Phone_Number = Column(String, nullable=False)
    Membership_Number = Column(Integer, nullable=False)

    def __repr__ (self):
        return self.Name

class Referee(Model):
    id = Column(Integer, primary_key=True)
    Name = Column(String, nullable=False)
    Phone_Number = Column(String, nullable=False)
    Membership_Number = Column(Integer, nullable=False)

    def __repr__ (self):
        return self.Name
class Admin(Model):
    id = Column(Integer, primary_key=True)
    FullName = Column(String, unique=True)
    EmployeeId = Column(Integer, unique=True)   

    def __repr__(self):
        return self.FullName


# nominee_helper = Table('Nominee', Base.metadata,
#                 Column('Nominee_id', Integer, ForeignKey('Nominee.id'), primary_key=True),
#                 Column('Member_id', Integer, ForeignKey('Member.id'), primary_key=True)
#                 )

# assoc_Referee_Member = Table('Member_referee', Model.metadata,
#                         Column('id', Integer, primary_key=True),
#                         Column('Referee_id', Integer, ForeignKey('Referee.id')),
#                         Column('Member_id', Integer, ForeignKey('Member.id'))
# )
 
class Member(Model):
    id = Column(Integer, primary_key=True)
    FirstName = Column(String(20), nullable= False)
    SecondName = Column(String(20), nullable= False)
    LastName = Column(String(20), nullable= False)
    IDNumber = Column(Integer, unique= True, nullable= False)
    Address = Column(String(50))
    Employer = Column(String(20))
    EmployerAddress = Column(String(20))
    Position = Column(String(10))
    WorkStation = Column(String(20))
    DateOfAppointment = Column(Date)
    GrossIncome = Column(Integer)
    PayrollNumber = Column(Integer)
    # Nominee = relationship('Nominee', secondary=nominee_helper, lazy='subquery', backref=backref('Members', lazy=True))
    # Referee = relationship("Referee")
    # Salary = Column(Boolean)
    # Pension = Column(Boolean)
    # Other = Column(String(20))
    # BusinessName=Column(String, nullable= True)
    # BusinessAddress=Column(String, nullable= True)
    # BusinessNature=Column(String, nullable= True)
    # BusinessLocation=Column(String, nullable= True)
    # BusinessIncome=Column(Integer, nullable= True)
    # Nominee=ForeignKey(Nominee, secondary= assoc_Nominee_Member, nullable = True)
    # Referee=ForeignKey(Referee, secondary= assoc_Referee_Member, nullable=True)


    def __repr__(self):
        return self.FirstName

    


class Nominee(Model):
    id = Column(Integer, primary_key=True)
    Name = Column(String, nullable=False)
    Phone_Number = Column(String, nullable=False)
    Membership_Number = Column(Integer, nullable=False)

    def __repr__ (self):
        return self.Name

class Referee(Model):
    id = Column(Integer, primary_key=True)
    Name = Column(String, nullable=False)
    Phone_Number = Column(String, nullable=False)
    Membership_Number = Column(Integer, nullable=False)

    def __repr__ (self):
        return self.Name

class Loan(Model):
    id=Column(Integer, primary_key=True)
    Amount = Column(Integer, nullable=False)
    # nominee = ForeignKey(Member) 
    