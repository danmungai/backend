                        Bank-dash
--------------------------------------------------------------

- Install the dependancies and requirements::

	pip install flask-appbuilder

- Run it::

    $ export FLASK_APP=app
    # Create a SuperAdmin user
    $ flask fab create-admin
    # Run the development server
    $ flask run


That's it!!
